package main

// Chant represents the collection of information that make up the chant
// In practice, it is the type given to the template to generate
// the LaTeX source
type Chant struct {
	Title     string
	Subtitle  string
	PointSize int
	ScorePath string
	Verses    []Verse
}

// Verse holds the two lines that make up
// an individual verse in the chant
type Verse struct {
	FirstPart  string
	SecondPart string
	IsSecond   bool
}
