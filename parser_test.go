package main

import (
	"testing"
)

func TestSimpleVerseExtraction(t *testing.T) {
	input := "abc\n123"
	verses := ParseInput(input)
	expected := []Verse{
		Verse{
			FirstPart:  "abc",
			SecondPart: "123",
		},
	}

	equals(t, expected, verses)
}

func TestLineBreakExtraction(t *testing.T) {
	input := "abc\n12\\\\3"
	verses := ParseInput(input)
	expected := []Verse{
		Verse{
			FirstPart:  "abc",
			SecondPart: `12\breaklongline{} 3`,
		},
	}

	equals(t, expected, verses)
}

func TestBracketExtraction(t *testing.T) {
	input := "a(bc)\n123"
	verses := ParseInput(input)
	expected := []Verse{
		Verse{
			FirstPart:  `a\bracket{bc}`,
			SecondPart: "123",
		},
	}

	equals(t, expected, verses)
}

func TestMultipleVerses(t *testing.T) {
	input := "abc\n123\n\ndef\n456"
	verses := ParseInput(input)
	expected := []Verse{
		Verse{
			FirstPart:  "abc",
			SecondPart: "123",
		},
		Verse{
			FirstPart:  "def",
			SecondPart: "456",
		},
	}

	equals(t, expected, verses)
}

func TestExtraLineBreaksBetweenVerses(t *testing.T) {
	input := "abc\n123\n\n\ndef\n456"
	verses := ParseInput(input)
	expected := []Verse{
		Verse{
			FirstPart:  "abc",
			SecondPart: "123",
		},
		Verse{
			FirstPart:  "def",
			SecondPart: "456",
		},
	}

	equals(t, expected, verses)
}

func TestNonEmptyLineBetweenVerses(t *testing.T) {
	input := "abc\n123\n   \ndef\n456"
	verses := ParseInput(input)
	expected := []Verse{
		Verse{
			FirstPart:  "abc",
			SecondPart: "123",
		},
		Verse{
			FirstPart:  "def",
			SecondPart: "456",
		},
	}

	equals(t, expected, verses)
}

func TestCaratSignalsSecondPart(t *testing.T) {
	input := "^abc\ndef"
	verses := ParseInput(input)
	expected := []Verse{
		Verse{
			FirstPart:  "abc",
			SecondPart: "def",
			IsSecond:   true,
		},
	}

	equals(t, expected, verses)
}

func TestMultipleBracketsOnSameLineExtraction(t *testing.T) {
	input := "a(bc) 1(23)\n456"
	verses := ParseInput(input)
	expected := []Verse{
		Verse{
			FirstPart:  `a\bracket{bc} 1\bracket{23}`,
			SecondPart: "456",
		},
	}

	equals(t, expected, verses)
}

func TestCaratSignalsSecondVerseWithPreviousVerses(t *testing.T) {
	input := "abc\n123\n\n^def\n456"
	verses := ParseInput(input)
	expected := []Verse{
		Verse{
			FirstPart:  "abc",
			SecondPart: "123",
		},
		Verse{
			FirstPart:  "def",
			SecondPart: "456",
			IsSecond:   true,
		},
	}

	equals(t, expected, verses)
}

func TestLaTeXReservedCharactersAreEscaped(t *testing.T) {
	input := `# $ % { } ~`
	verses := ParseInput(input)
	expected := []Verse{
		Verse{
			FirstPart: `\# \$ \% \{ \} \~`,
		},
	}

	equals(t, expected, verses)
}

func TestLordIsSmallCapped(t *testing.T) {
	input := "Lord\nLORD"
	verses := ParseInput(input)
	expected := []Verse{
		Verse{
			FirstPart:  `\textsc{Lord}`,
			SecondPart: `\textsc{Lord}`,
		},
	}

	equals(t, expected, verses)
}
