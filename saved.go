package main

import (
	"bytes"
	"encoding/json"
	"io"
)

// Saved holds the user input from a previous run
type Saved struct {
	ScorePath string
	ScoreName string
	Chant     string
	Title     string
	Subtitle  string
}

// LoadSaved loads the saved chant from a given io.Reader
func LoadSaved(chant io.Reader) Saved {
	var savedMap map[string]string
	buf := bytes.NewBuffer([]byte{})
	buf.ReadFrom(chant)
	json.Unmarshal(buf.Bytes(), &savedMap)
	return Saved{
		Title:     savedMap["title"],
		Subtitle:  savedMap["subtitle"],
		ScorePath: savedMap["score"],
		ScoreName: savedMap["scoreName"],
		Chant:     savedMap["chant"],
	}
}

// SaveInput saves the submitted input to a given io.Writer
func SaveInput(f io.Writer, title, subtitle, chant, scoreName, scorePath string) error {
	jsMap := map[string]string{
		"title":     title,
		"subtitle":  subtitle,
		"chant":     chant,
		"scoreName": scoreName,
		"score":     scorePath,
	}
	js, err := json.Marshal(jsMap)
	if err != nil {
		return err
	}

	_, err = f.Write(js)
	return err
}
