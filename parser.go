package main

import (
	"regexp"
	"strings"
)

var re = regexp.MustCompile(`\((.*?)\)`)

const (
	// FIRST signals to the parser that it is looking
	// for a line in the first part of the verse
	FIRST = iota
	// SECOND signals to the parser that it is looking
	// for a line in the second part of the verse
	SECOND
)

// ProcessLine takes a line and returns a "processed" version
// (i.e., one that uses the TeX commands instead of the user inputted ones)
func ProcessLine(line string) string {
	// Escape LaTeX reserved characters
	line = strings.Replace(line, "#", `\#`, -1)
	line = strings.Replace(line, "$", `\$`, -1)
	line = strings.Replace(line, "%", `\%`, -1)
	line = strings.Replace(line, "{", `\{`, -1)
	line = strings.Replace(line, "}", `\}`, -1)
	line = strings.Replace(line, "~", `\~`, -1)
	// Format interline breaks
	line = strings.Replace(line, `\\`, `\breaklongline{} `, -1)
	// Format brackets over words
	line = re.ReplaceAllString(line, `\bracket{${1}}`)
	// Smallcap "Lord"
	line = strings.Replace(line, "Lord", `\textsc{Lord}`, -1)
	line = strings.Replace(line, "LORD", `\textsc{Lord}`, -1)
	return line
}

// ParseInput converts the raw input string into a collection of verses while also
// changing the user input formatting strings into the TeX-sanctioned commands
func ParseInput(input string) (verses []Verse) {
	lines := strings.Split(input, "\n")

	v := Verse{}
	parity := FIRST

	if len(lines) == 1 {
		if strings.HasPrefix(lines[0], "^") {
			return []Verse{
				Verse{
					FirstPart: ProcessLine(lines[0][1:]),
					IsSecond:  true,
				},
			}
		}
		return []Verse{
			Verse{
				FirstPart: ProcessLine(lines[0]),
			},
		}
	}

	for _, l := range lines {
		// Empty lines are used to separate verses
		if strings.TrimSpace(l) == "" {
			parity = FIRST
			continue
		}
		line := ProcessLine(l)
		if strings.HasPrefix(line, "^") {
			v.IsSecond = true
			line = line[1:]
		}

		if parity == FIRST {
			v.FirstPart = line
			parity = SECOND
		} else if parity == SECOND {
			v.SecondPart = line
			verses = append(verses, v)
			parity = FIRST
			v = Verse{}
		}
	}

	return verses
}
