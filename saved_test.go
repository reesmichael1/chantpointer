package main

import (
	"bytes"
	"testing"
)

var savedJSON = `{"chant":"line 1\nline 2\n\nab(c 1)23\nword | \"word\"","score":"/tmp/abc/chant.png","scoreName":"fake_name.png","subtitle":"Psalm Subtitle","title":"Psalm Title"}`

func TestSaveChant(t *testing.T) {
	buf := bytes.NewBuffer([]byte{})

	SaveInput(buf, "Psalm Title", "Psalm Subtitle", `line 1
line 2

ab(c 1)23
word | "word"`, "fake_name.png", "/tmp/abc/chant.png")

	equals(t, savedJSON, string(buf.Bytes()))
}

func TestLoadSavedChant(t *testing.T) {
	buf := bytes.NewBufferString(savedJSON)
	s := LoadSaved(buf)

	expected := Saved{
		Chant: `line 1
line 2

ab(c 1)23
word | "word"`,
		Title:     "Psalm Title",
		Subtitle:  "Psalm Subtitle",
		ScorePath: "/tmp/abc/chant.png",
		ScoreName: "fake_name.png",
	}

	equals(t, expected, s)
}
