package main

import (
	"io"
	"text/template"
)

// WriteTexForChant writes the given Chant to an io.Writer
// (in practice, a text file, or for testing, an in-memory buffer)
func WriteTexForChant(f io.Writer, c *Chant) error {
	templatePath := "psalm.tem"
	t, err := template.New(templatePath).Delims("((", "))").ParseFiles(templatePath)
	if err != nil {
		return err
	}

	err = t.Execute(f, c)
	if err != nil {
		return err
	}

	return nil
}
