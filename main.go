package main

import (
	"log"
	"net/http"
)

func main() {
	static := http.FileServer(http.Dir("static"))
	http.Handle("/static/", http.StripPrefix("/static", static))
	http.HandleFunc("/chant/", ChantHandler)
	http.Handle("/generate", ErrorHandler(GenerateHandler))
	http.Handle("/", ErrorHandler(IndexHandler))
	log.Fatal(http.ListenAndServe(":8080", nil))
}
