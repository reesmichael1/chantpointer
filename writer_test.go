package main

import (
	"bytes"
	"testing"
)

var savedTeX = `\documentclass[12pt]{article}

\usepackage[margin=1in]{geometry}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{psalm}

\pagestyle{empty}

\begin{document}

\begin{center}
    {\Large \textbf{Title}}\\[12pt]
    \textit{Subtitle}\\[30pt]
    \includegraphics[width=\textwidth]{/tmp/chant.png}
\end{center}
\vspace{24pt}

\setlength\textamount{12cm}

\begin{center}
\begin{psalm}


\vs{}{abc}
{}{def}


\vs{}{123}
{}{456}


\vs{\second}{ghi}
{}{789}


\end{psalm}
\end{center}

\end{document}
`

func TestWriteVerses(t *testing.T) {
	buf := bytes.NewBuffer([]byte{})

	c := Chant{
		Title:    "Title",
		Subtitle: "Subtitle",
		Verses: []Verse{
			Verse{
				FirstPart:  "abc",
				SecondPart: "def",
			},

			Verse{
				FirstPart:  "123",
				SecondPart: "456",
			},
			Verse{
				FirstPart:  "ghi",
				SecondPart: "789",
				IsSecond:   true,
			},
		},
		PointSize: 12,
		ScorePath: "/tmp/chant.png",
	}

	WriteTexForChant(buf, &c)
	equals(t, savedTeX, string(buf.Bytes()))
}
